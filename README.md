# GNOME Badging System

Welcome to the GNOME Badging System repository!

This repository serves as the central hub for managing the recognition of contributors, volunteers, maintainers, interns accross various GNOME technologies and events through a badging system.

## About GNOME Badging System

The GNOME Badging System project aims to foster a culture of appreciation and recognition within the GNOME community by awarding badges to individuals who make contributions to the ecosystem. Whether you're a developer, designer, translator, tester, intern, volunteer, staff your efforts are valued and acknowledged through this badging system.

This system extends recognition to various roles within the GNOME community, including maintainers of GNOME technologies, volunteers at community events, interns participating in GNOME Community initiatives, and contributors across diverse domains. By acknowledging the diverse contributions that enrich the GNOME ecosystem, we strive to create an inclusive environment where every effort is celebrated and valued.

## Get Involved
- **Request a Badge:** If you've made contributions to the GNOME community and believe you're eligible for a badge, create an issue in the issues section following the guidelines provided.
- **Review Requests:** If you're a maintainer, help review badge requests and ensure deserving contributors receive recognition for their efforts.
- **Spread the Word:**  Help promote the GNOME Badging System within the community and encourage participation and engagement.

## Contributing

## Contact
If you have any questions, suggestions, or feedback regarding the GNOME Badging System, please reach out to us via [email](africa@gnome.org) or [issue tracker](https://gitlab.gnome.org/regina_nkenchor/gnome-badging-system/-/issues)

This README provides an overview of the badging system, including how to request badges, criteria for earning badges, badges category and guidelines for contributing.


